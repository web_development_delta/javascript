// js part3 assignment
//Q.1 write the js program to get first n elements of an array.
const arr = [1, 2, 3, 4]
const n = 2
console.log(arr.splice(0, n))

//Q.2 write the js program to find last n number of array

console.log(arr.splice(arr.length - n))

// Q.3 write the js program to check wheather the string is blank or not

let str = prompt('please enter the string')
if (str.length === 0) {
  console.log('string is blank string.')
} else {
  console.log('string is not blank')
}

//Q.4 write the js program to test wheather the character at given index is lower case.
let str1 = 'AbCdEfGhIjK'
let index = 4
if (str1[index] == str1[index].toLowerCase()) {
  console.log('LowerCase')
} else {
  console.log('UpperCase')
}

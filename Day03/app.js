console.log('Hello Hell')
/*
// declare an array

let arr = [1, 2, 3, 4, 5, 6]
console.log(arr)
// array indexing starts from 0 to the number
// to find the length of an array
let size = arr.length
console.log(size)
console.log(`The length of an array is ${size}`)
console.log('__________________________________________')
// array methods

let cars = ['audi', 'bmw', 'maruti']
console.log(cars)
cars.push('suv')
console.log(cars)
cars.pop()
console.log(cars)
cars.unshift('alto')
console.log(cars)
cars.shift()
console.log(cars)
*/

// from the given start state of an array ,change it to the final form using method.
// start : ['january','july','march','august']

// final : ['july','june','march','august']

let months = ['january', 'july', 'march', 'august']
console.log(months)
months.shift()
console.log(months)
months.pop()
months.push('june')
months.push('july')
console.log(months)

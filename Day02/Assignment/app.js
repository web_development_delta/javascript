// Q1. create the number variable num with some value
// now print 'good' if the number is divisible by 10 and print bad of not.

/*
let number = 101
if (number % 10 == 0) {
  console.log('Good')
} else {
  console.log('Bad')
}
*/

// Q2. take the user's name & age as the input using prompts.
// then return back the following  statement to use as an alert
// by substuting their name and age ):
// name is age years old.
//[use templet literal]

/*
let firstName = prompt('Enter your Name :')
let age = prompt('Enter your age :')
alert(`${firstName} is ${age} years old. `)
*/

// Q.3 Write the switch statements to print the months in quarter
//MonthsinQuarter1:January,February,March
//MonthsinQuarter2:April,May,June
//MonthsinQuarter3:July,August,September
//MonthsinQuarter4:October,November,December

// use number as the case value in switch
/*
let quarter = 3
switch (quarter) {
  case 1:
    console.log('January,February,March')
    break
  case 2:
    console.log('April,May,June')
    break
  case 3:
    console.log('July,August,September')
    break
  case 4:
    console.log('October,November,December')
    break
  default:
    console.log('Pls enter value of quarter between 1 to 4')
}

*/

// Q. 4 A string is the Golden String if it starts with the character 'A' or 'a'
// and has total length greater than 5
// for the given string print if it is golden or not.

/*
let str = 'Apple'
if ((str[0] == 'A' || str[0] == 'a') && str.length >= 5) {
  console.log('Golden')
} else {
  console.log('Bad')
}

*/

// Q.5 write the program to find the largest of the 3 numbers
/*
let num1 = 66
let num2 = 22
let num3 = 33
if (num1 > num2) {
  if (num1 > num3) {
    console.log(`${num1} is largest`)
  }
} else if (num2 > num3) {
  if (num2 > num1) {
    console.log(`${num2} is largest`)
  }
} else {
  console.log(`${num3} is the largest`)
}
*/

// Q.6 [Bonus Question]:
// write the program to check if two numbers have same last digit
// eg. 32 and 35352 have the same last digit . i.e 2

let num1 = 1232
let num2 = 143427

let lastDigit1 = num1 % 10
let lastDigit2 = num2 % 10

if (lastDigit1 == lastDigit2) {
  console.log(
    `${num1} and ${num2} have same digit at the last which is  ${lastDigit1}`
  )
} else {
  console.log(`${num1} and ${num2} Do not have same last digit`)
}

# Quolam Business Solution

## Questions

1. What reasons made you apply for thr role of Junior Frontend Developer at Quolam Business Solutions ?

- I would like to apply for these position beacause I am seeking a more challenging opportunity in my field.
- First of all Quolam Business solution is the digital age company that helps the customer to build the quick and high impact digital experience.
- I enjoy the creative outlet it provides and the opportunity to put my programming skills to work at Quolam Business Solutions.
- Quolam Business Solution is the startup started on 2015 it provides the services like Web & Mobile, DevOps, Data and Cloud solutions to the customer it will definetly helps me for my future career growth.
- As I am profishient at HTML, CSS & JS and nodejs so this job profile is sutaible for me .

1. Explain what are the skills that you possess and how you can help us in our projects ?

- Proficiency in HTML, CSS, and JavaScript these are the core languages of frontend development.
- Understand responsive web design principle and create mobile friendly layouts.
- knowledge of front-end development framework like Bootstrap.
- knowledge of in-demand frontend library ReactJs.
- I have design project in HTML,CSS like Amazon clone website.
- I have strong knowledge on database like MySql Database.
- Develop MERN stack web application using Mysql Database, ExpressJs server, ReactJs Frontend Library, NodeJs Framework.
- I know Git(VCS) I use git on everyday basis. Familier with all git commands.
- I have knowledge of software engineering and software development principles that will me to understand the project design and flow.
- GitHub link - https://github.com/karankhutwad/CSS-Project
- GitLab Link - https://gitlab.com/d7_62907_karan_khutwad
